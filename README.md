# moodle clustering and load balansing with nginx 

1.install docker in the envirnment please follow this document to install docker 

https://docs.docker.com/install/

2.install docker compose please follow following document to install docker composer

https://docs.docker.com/compose/install/#uninstallation

checkout the git repository

cd moodle 

# change the ip address of local server in nginx.conf 

upstream myproject {
    server 172.19.0.1:8081;//change ip to service host name or service ip
    server 172.19.0.1:8082;
    keepalive 64;
  }
  
# after that run followin commands 

docker-compose build 

docker-compose up 

# stop docker containers

docker-compose down

